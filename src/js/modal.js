// **************************************************
// Author: Jonathan Harrison
// Date: 24/05/2021
// Multi Modal interaction script v1.0
// **************************************************

// Find and store all modal elements for opening and closing
let modalBtns = document.querySelectorAll(".modal--open");
let cancelBtns = document.querySelectorAll(".modal--cancelBtn");

/* Iterate through modals and set display by matching modal ID (i.e. Modal 1) 
Allows multiple modals to be easily created so long as unique ID and data-modal type match.*/
modalBtns.forEach(function(btn) {
    btn.onclick = function() {
        let modal = btn.getAttribute('data-modal');
        document.getElementById(modal).style.display = "block";
    };
});


// Iterate through all modal close buttons and close open modal
cancelBtns.forEach(function(btn) {
    btn.onclick = function() {
        let modal = (btn.closest(".modal").style.display = "none");
    };
});

// Close modal if anywhere else on the site is clicked
window.onclick = function(e) {
    if (e.target.className === "modal") {
        e.target.style.display = "none";
    }
};