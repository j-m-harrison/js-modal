# JS Multi-Modal Demo with Sass

[![Build Status](https://travis-ci.org/techlab23/task-management-app.svg?branch=master)](https://travis-ci.org/techlab23/task-management-app) [![Coverage Status](https://coveralls.io/repos/github/techlab23/task-management-app/badge.svg)](https://coveralls.io/github/techlab23/task-management-app)

**Simple Multi Modal Demo Page I developed for a code assessment for a front end developer role**

## Setup

You�ll start by running the index.html in live server and clicking the buttons to open individual modals.

1. Page content is fully provided.
2. HTML5 is used with semantic tags for elements and structure.
3. Sass precompiled CSS3 is used for styling the page and modals with animations and transitions.
4. The modals are fired and managed via a separate main.js script

---

## How to add modals

The page was designed to support multiple modals with an easy method to do so.

1. Add modal with the modal class and an ID with the modal name to the HTML file (See existing examples).
2. Give the modal a data-modal attribute of the same name as the ID. (This is for the script to be able to iterate through and enumerate unique modals).
3. Add a button or link to fire the modal by linking it via this data-modal attribute on the open-modal function. (Again see examples provided in demo).
4. Open in LiveServer/Browser and enjoy your new modals.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the wireframe pdf for its design and behaviors below.

---

## Designs

[Wireframe here](docs/jet2ModalDesign.pdf)

---

## Screenshots

![Modal Image](docs/images/modal-screenshot.png)

![Modal Image Focused](docs/images/modal-focused-screenshot.png)

---

## Check out my other work

[portfolio here](https://portfolio-site-a770f.web.app/).